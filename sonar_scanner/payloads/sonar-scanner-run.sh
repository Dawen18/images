#!/bin/sh

ERROR=0

# Check user defined variables
if [[ -z "${SONAR_URL}" ]]; then
  echo "Undefined \"SonarQube URL (variable SONAR_URL)\"" && ERROR=1
fi

if [[ -z "${SONAR_USER}" ]]; then
  echo "Undefined \"SonarQube Login (variable SONAR_USER)\"" && ERROR=1
fi

if [[ -z "${GITLAB_USER_TOKEN}" ]]; then
  echo "Undefined \"GitLab user token for sonar-gitlab-plugin (variable GITLAB_USER_TOKEN)\"" && ERROR=1
fi

if [[ -z "${PROJECT_VERSION}" ]]; then
  echo "Undefined \"GitVersion project version (variable PROJECT_VERSION)\"" && ERROR=1
fi

# Gitlab defined variables
if [[ -z "${CI_PROJECT_ID}" ]]; then
  echo "Undefined \"GitLab project ID (variable CI_PROJECT_ID)\"" && ERROR=1
fi

if [[ -z "${CI_PROJECT_NAME}" ]]; then
  echo "Undefined \"GitLab project name (variable CI_PROJECT_NAME)\"" && ERROR=1
fi

if [[ -z "${CI_COMMIT_SHA}" ]]; then
  echo "Undefined \"GitLab commit reference (variable CI_COMMIT_SHA)\"" && ERROR=1
fi

if [[ -z "${CI_COMMIT_REF_NAME}" ]]; then
  echo "Undefined \"GitLab commit reference name (variable CI_COMMIT_REF_NAME)\"" && ERROR=1
fi

if [[ ${ERROR} -ne 0 ]]; then
    exit 1;
fi

# Install dependencies if angular
if [[ -f "angular.json" ]]; then
    npm install
fi

# Authentication
COMMAND="sonar-scanner -Dsonar.host.url=${SONAR_URL} -Dsonar.login=${SONAR_USER}"

# Project identity
COMMAND="${COMMAND} -Dsonar.projectKey=gitlab_${CI_PROJECT_ID}"
COMMAND="${COMMAND} -Dsonar.projectName=\"${CI_PROJECT_NAME}\""
COMMAND="${COMMAND} -Dsonar.projectVersion=${PROJECT_VERSION}"

# Gitlab specific information
COMMAND="${COMMAND} -Dsonar.gitlab.url=https://gitlab.com"
COMMAND="${COMMAND} -Dsonar.gitlab.user_token=${GITLAB_USER_TOKEN}"
COMMAND="${COMMAND} -Dsonar.gitlab.project_id=${CI_PROJECT_ID}"
#ALL_MERGED_COMMITS=$(git log --pretty=format:%H origin/master..${CI_COMMIT_SHA} | tr '\n' ',')
#COMMAND="${COMMAND} -Dsonar.gitlab.commit_sha=${ALL_MERGED_COMMITS}"
COMMAND="${COMMAND} -Dsonar.gitlab.commit_sha=${CI_COMMIT_SHA}"
COMMAND="${COMMAND} -Dsonar.gitlab.ref_name=${CI_COMMIT_REF_NAME}"

# Setting exit status as failure notification for GitLab-CI
COMMAND="${COMMAND} -Dsonar.gitlab.failure_notification_mode=commit-status"
COMMAND="${COMMAND} -Dsonar.gitlab.quality_gate_fail_mode=WARN"
if [ -n "$DEBUG" ]; then
    COMMAND="$COMMAND -X"
fi

# User defined arguments
COMMAND="${COMMAND} $@"

eval "${COMMAND}"