#!/usr/bin/env bash

CI_COMMIT_MESSAGE=${CI_COMMIT_MESSAGE/-/_}
CI_COMMIT_MESSAGE=${CI_COMMIT_MESSAGE^^}

if [[ ${CI_COMMIT_MESSAGE} == *"INC_MAJOR"* ]]; then
  echo "increment major"
  CMD=inc_major
elif [[ ${CI_COMMIT_MESSAGE} == *"INC_PATCH"* ]]; then
  echo "Increment patch"
  CMD=inc_patch
else
  echo "Increment minor"
  CMD=inc_minor
fi

while IFS= read -r DIR; do
  VERSION=$(registry_explorer ${CMD} -i "${DIR}" -r 14806829)
  if [ -z "$VERSION" ]; then
    VERSION=0.1.0
  fi

  echo "export VERSION_${DIR^^}=${VERSION}" >> image_versions
done < <(find . -maxdepth 1 -mindepth 1 -type d -not -path '*/\.*' -printf '%f\n')
