# Docker images

Project for storage of different docker images

## Images

### android_sdk

Openjdk alpine image with android sdk.

installed packages:
* wget 
* unzip 
* ca-certificates 
* bash 
* git 
* jq

### gitversion

Image `alpine:latest` with following packages:
* git
* python2
* mono
* [gitversion](https://gitversion.readthedocs.io/en/latest/)

### node_jq

Image `node:lts-alpine` with jq.

### sonar_scanner

Image `openjdk:alpine` with [SonarQube](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/) analyser installed.
Runner shell script exist for GitLab CI run. See [sonar-scanner-run.sh](sonar_scanner/payloads/sonar-scanner-run.sh) file.

### symfony

Image `php:fpm-alpine` with following packages:
* git
* jq
* zip
* nodejs

And following PHP extensions:
* intl
* zip
* opcache
* calendar
* pdo pdo_mysql
* gd
* xdebug

Following tools are also installed:
* composer
* symfony installer

## Add a new image

1. To add a new image, create a new directory, the directory name **MUST** be the image name and 
put your Dockerfile inside this directory.
2. Edit [`.gitlab-ci.yml`](.gitlab-ci.yml) file and apply changes as explained below.
3. Add image description in this file.

### Add build image job

Add a job as below. Replace `<IMG_NAME>` by your created directory who is your image name.

```yaml
build:<IMG_NAME>:
  extends: .build-image
  variables:
    IMAGE_NAME: <IMG_NAME>
    DOCKER_FLAGS: # Put your docker flag here. For example --build-arg TOTO=1234. Can be removed if not needed.
  only:
    refs:
      - master
    changes:
      - <IMG_NAME>/*
```

### Update image

Image versioning are automated. By default, the minor number are incremented. If you want to increment the patch number,
add `inc_patch` in your commit message. If you want to increment major, add `inc_major` in your commit message.

### Multi project pipeline

To build image with multi project pipeline, add a job like below with `<IMG_NAME>` as your image name.

```yaml
update:<IMG_NAME>:
  extends: .build-image
  variables:
    IMAGE_NAME: <IMG_NAME>
  only:
    refs:
      - pipelines
    variables:
      - $TRIGGER_JOB == "<IMG_NAME>"
```

And in upstream pipeline, add a job like below.

```yaml
update-image:
  only:
    - tags
  variables:
    TRIGGER_JOB: <IMG_NAME>
  stage: deploy
  trigger: 2CS/docker-images
```